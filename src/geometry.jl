using Base: @kwdef

@kwdef struct Geometry
    α::Float64
    β::Float64
end

const PLANAR = Geometry(α = 0.0, β = 1.0)
const SPHERICAL = Geometry(α = 2.0, β = 4π)
const CYLINDRYCAL = Geometry(α = 1.0, β = 2π)
