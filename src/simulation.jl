using Dates
using HDF5

include("hydro-driver.jl")

# create outputs directory
OUTPUT_DIR = "./out"

mutable struct Simulation
    hydro_driver::HydroDriver
    name::String
    dt_writing::Float64
    h5filename::String
    writing_index::Int64
    time::Float64
end

# constructor
function Simulation(;
    name::String,
    geometry::Geometry,
    γ::Float64,
    initial_position::Vector{Float64},
    initial_velocity::Vector{Float64},
    initial_pressure::Vector{Float64},
    initial_density::Vector{Float64},
    dt_writing::Float64,
    dt_initial_max::Float64,
    dt_factor::Float64 = 1.1,
    σ_artificial_viscosity::Float64 = 1.0,
    dt_density_ε::Float64 = 1e-2,
    dt_courant_factor::Float64 = 0.25,
)::Simulation

    Base.Filesystem.mkpath(OUTPUT_DIR)

    now_ = Dates.format(now(), "yyyy-mm-dd_HH-MM-SS")
    simulation = Simulation(
        HydroDriver(;
            geometry = geometry,
            γ = γ,
            initial_position = initial_position,
            initial_velocity = initial_velocity,
            initial_pressure = initial_pressure,
            initial_density = initial_density,
            σ_artificial_viscosity = σ_artificial_viscosity,
            dt_initial_max = dt_initial_max,
            dt_factor = dt_factor,
            dt_density_ε = dt_density_ε,
            dt_courant_factor = dt_courant_factor,
        ),
        name,
        dt_writing,
        "$(OUTPUT_DIR)/$(name)_$(now_).h5",
        0,
        0.0,
    )

    initialize_hdf5(simulation)
    write_to_h5!(simulation)

    simulation
end

function initialize_hdf5(simulation::Simulation)::Simulation
    number_of_cells = length(simulation.hydro_driver.data.mass)
    h5open(simulation.h5filename, "cw") do file
        attributes(file)["name"] = simulation.name
    end
    simulation
end

function write_to_h5!(simulation::Simulation)::Simulation
    vars = Dict(
        "position" => simulation.hydro_driver.data.position,
        "velocity" => simulation.hydro_driver.data.velocity,
        "pressure" => simulation.hydro_driver.data.pressure,
        "density" => simulation.hydro_driver.data.density,
        "artificial_viscosity" => simulation.hydro_driver.data.artificial_viscosity,
    )
    h5open(simulation.h5filename, "cw") do file
        g = create_group(file, "$(simulation.writing_index)")
        attributes(g)["time"] = simulation.time
        for (key, value) ∈ vars
            g[key] = value
        end
    end
    simulation.writing_index += 1
    simulation
end

function mainloop!(simulation::Simulation; max_time::Float64)
    percentage::Int64 = last_percentage::Int64 = 0
    last_writing_time::Float64 = 0.0

    println("0%")

    while simulation.time < max_time

        # Print percentage
        percentage = trunc(Int, 100 * simulation.time / max_time)
        if percentage > last_percentage
            println("$percentage%")
            last_percentage = percentage
        end

        simulation.time += step!(simulation.hydro_driver).dt

        if simulation.time - last_writing_time ≥ simulation.dt_writing
            write_to_h5!(simulation)
            last_writing_time = simulation.time
        end
    end

    println("Done!")
    println("Output file is at $(simulation.h5filename)")
end
