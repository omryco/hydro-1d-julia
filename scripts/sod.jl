using Hydro1D

name = "Sod"
geometry = PLANAR

γ = 1.4
L = 1.0
t_max = 0.2
num_of_cells = 1000

density_left = 1.0
pressure_left = 1.0
density_right = 0.125
pressure_right = 0.1

position = collect(range(0.0, L; length = num_of_cells + 1))
velocity = zeros(Float64, num_of_cells + 1)
mid_position = (position[2:end] + position[1:end-1]) / 2.0

pressure = [pos ≤ L / 2.0 ? pressure_left : pressure_right for pos ∈ mid_position]
density = [pos ≤ L / 2.0 ? density_left : density_right for pos ∈ mid_position]

simulation = Simulation(
    name = name,
    geometry = geometry,
    γ = γ,
    initial_position = position,
    initial_velocity = velocity,
    initial_pressure = pressure,
    initial_density = density,
    dt_writing = 1e-3,
    dt_initial_max = 1e-8 * t_max,
)

mainloop!(simulation, max_time = t_max)
